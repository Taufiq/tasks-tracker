import { Module } from '@nestjs/common';
import { UserModule } from './web/modules/user.module';
import { TaskModule } from './web/modules/task.module';
import { GraphQLModule } from '@nestjs/graphql';
import GraphQLJSON from 'graphql-type-json';
import { join } from 'path';
import { EasyconfigModule, EasyconfigService } from 'nestjs-easyconfig';
import { DatabaseModule } from './web/modules/database.module';

@Module({
  imports: [
    EasyconfigModule.register({ path: './.env', safe: true }),
    GraphQLModule.forRoot({
      installSubscriptionHandlers: true,
      autoSchemaFile: join(process.cwd(), 'schema.gql'),
      sortSchema: true,
      resolvers: { JSON: GraphQLJSON },
      context: ({req}) => ({req}),
      path: 'tracker',
    }),
    DatabaseModule,
    UserModule,
    TaskModule,
  ],
  providers: [],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly config: EasyconfigService) {
    AppModule.port = this.config.get('API_PORT');
  }
}
