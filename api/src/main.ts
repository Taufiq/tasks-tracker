import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('/tracker');
  app.enableCors();
  await app.listen(AppModule.port);
  console.log('Api listen in port: ', AppModule.port);
}
bootstrap();
