import { registerEnumType } from "@nestjs/graphql";

export enum StatusTask {
    ACTIVE = "Active",
    PAUSED = "Paused",
    FINISHED = "Finished"
  }
  
registerEnumType(StatusTask, {
   name: 'StatusTask',
   description: 'Posibles estados de las tareas',
});

export enum TaskType {
    PROJECT = "Project",
    TASK = "task"
}
  
registerEnumType(TaskType, {
   name: 'TaskType',
   description: 'Tipos de tareas',
});