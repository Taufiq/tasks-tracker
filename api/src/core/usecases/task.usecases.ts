import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { TaskService } from 'src/data/services/task.service';
import { UserService } from 'src/data/services/user.sevice';
import { TimeUtilities } from 'src/utils/timeUtilities';
import { StatusTask, TaskType } from '../enums/enums';

import { InputTaskType, Task, UpdateTaskType } from '../models/task.entity';

@Injectable()
export class TaskUseCases {

  constructor(
      private readonly _taskService: TaskService,
      private readonly _userService: UserService
  ) {}


  async getTask(idTask:number): Promise<Task>{
    return await this._taskService.get(idTask)
  }


  async getTasks(taskType: TaskType): Promise<Task[]>{
    if(taskType)
        return await this._taskService.getAllByType(taskType);
    else
        return await this._taskService.getAll()
  }


  async createTask( newTask: InputTaskType) : Promise<Task>{
    let taskFinded = await this._taskService.get(newTask.id);
    if(taskFinded) throw new BadRequestException(`Task ${newTask.id} exist`);
    const taskToSave = plainToClass(Task,newTask);
    return this._taskService.save(taskToSave);
  }


  async updateTask(inputTask:UpdateTaskType):Promise<Task>{

    if(!inputTask.id) throw new BadRequestException(`There is no id for task,to update needs an task id`);

    let task = await this._taskService.get(inputTask.id);
    if(!task) throw new NotFoundException(`Task ${inputTask.id} doesn't exist`);
    task = plainToClass(Task,inputTask);
    return await this._taskService.save(task);
  }


  async changeTaskStatus(id : number):Promise<Task>{

    let taskFinded = await this._taskService.get(id);
    if(!taskFinded) throw new NotFoundException(`Task ${id} doesn't exist`);
    if(taskFinded.type===TaskType.PROJECT) 
        throw new BadRequestException("You can't change the project status directly, so you need asign a sub task and then can manipulate the status of this sub-task");
    
    if(!taskFinded.status){
      taskFinded.status = StatusTask.ACTIVE;
      taskFinded.startDate = new Date();
    }else if (taskFinded.status===StatusTask.ACTIVE){
      const timeUtils = new TimeUtilities();
      taskFinded.status= StatusTask.PAUSED;
      taskFinded.currentTrack = new Date();
      taskFinded.progressTime =await timeUtils.getTimeInterval(taskFinded.startDate, taskFinded.currentTrack);
    }else if (taskFinded.status===StatusTask.PAUSED){
      taskFinded.status= StatusTask.ACTIVE;
    }

    let taskSaved = await this._taskService.save(taskFinded);
    return  taskSaved;    
  }


  async asignTaskToProject(idTask:number,idProject:number):Promise<Task>{

    let taskFinded = await this._taskService.get(idTask);
    if(taskFinded.fatherTask){
      throw new BadRequestException("Task has been asigned to another project previously");
    }
    
    let projectFinded = await this._taskService.get(idProject);
    if(!projectFinded){
      throw new NotFoundException("Project doesn't exist");    
    }

    taskFinded.fatherTask = projectFinded;
    return await this._taskService.save(taskFinded);
  }


  async asignTaskToUser(idTask:number,idUser:number):Promise<Task>{

    let taskFinded = await this._taskService.get(idTask);
    if(taskFinded.asignedTo){
      throw new BadRequestException("Task has been asigned to another user previously");
    }
    
    let userFinded = await this._userService.get(idUser);
    if(!userFinded){
      throw new NotFoundException("User doesn't exist");    
    }

    taskFinded.asignedTo = userFinded;
    return await this._taskService.save(taskFinded);   
  }
  
}