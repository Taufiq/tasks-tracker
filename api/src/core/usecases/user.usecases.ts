import { Injectable, NotFoundException } from '@nestjs/common';
import { UserService } from 'src/data/services/user.sevice';

import { InputUserType, User } from '../models/user.entity';

@Injectable()
export class UserUseCases {
  constructor(private readonly _userService: UserService) {}


  async getUser(idUser:Number): Promise<User>{
    return await this._userService.get(idUser)
  }

  async getAllUsers(): Promise<User[]>{
    return await this._userService.getAll()
  }

  async createUser( newUser: InputUserType) : Promise<User>{
    return this._userService.create(newUser)
  }
  
}