import { Field, InputType, Int, ObjectType } from "@nestjs/graphql";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { StatusTask, TaskType } from "../enums/enums";
import { InputUserType, User } from "./user.entity";
import JSON from 'graphql-type-json';

@Entity('tasks')
@ObjectType()
export class Task{

    @Field(()=> Int)
    @PrimaryGeneratedColumn('increment')
    id!: number;

    @Field(()=>String)
    @Column({type:'varchar',unique:true})
    name!: string;

    @Field(()=>String,{nullable:true})
    @Column({type:'text',nullable:true})
    description!: string;

    @Field(()=>JSON,{nullable:true,description:"json with 3 fields hr-min-sec"})
    @Column({type:'json',nullable:true,name:'stimated_time'})
    stimatedTime!: JSON;

    @Field(()=>JSON,{nullable:true,description:"json with 3 fields hr-min-sec"})
    @Column({type:'json',nullable:true,name:'progress_time'})
    progressTime!: JSON;

    @Field(()=>Date,{nullable:true})
    @Column({type:'timestamp',nullable:true,name:'start_date'})
    startDate!: Date;

    @Field(()=>Date,{nullable:true})
    @Column({type:'timestamp',nullable:true,name:'current_track'})
    currentTrack!: Date; 

    @Field(()=>StatusTask,{nullable:true})
    @Column({type:'enum',enum:StatusTask, nullable:true})
    status!: StatusTask;

    @Field(()=>TaskType)
    @Column({type:'enum',enum:TaskType})
    type!: TaskType;

    @Field(()=>User,{nullable:true})
    @ManyToOne(()=>User, user=>user.tasks)
    asignedTo: User;

    @Field(()=>Task,{nullable:true})
    @ManyToOne(()=>Task, task=>task.subTasks)
    fatherTask: Task;

    @Field(()=>[Task],{nullable:true})
    @OneToMany(()=>Task, task=>task.fatherTask)
    subTasks: [Task];

}


@InputType()
export class InputTaskType{

    @Field(()=> Int,{nullable:true})
    id!: number;

    @Field(()=>String,{nullable:true})
    name : string;

    @Field(()=>String,{nullable:true})
    description : string;

    @Field(()=>TaskType)
    type!: TaskType;

    @Field(()=>JSON,{nullable:true,description:"json with 3 fields hr-min-sec"})
    stimatedTime!: JSON;

    @Field(()=>InputUserType,{nullable:true})
    asignedTo : User;

    @Field(()=>InputTaskType,{nullable:true})
    fatherTask : Task;
}


@InputType()
export class UpdateTaskType{

    @Field(()=> Int,{nullable:true})
    id!: number;

    @Field(()=>String,{nullable:true})
    name : string;

    @Field(()=>String,{nullable:true})
    description : string;

    @Field(()=>JSON,{nullable:true,description:"json with 3 fields hr-min-sec"})
    stimatedTime!: JSON;
}
