import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  Column,
  Unique,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Task } from './task.entity';


@Entity('users')
@Unique(['user_name'])
@ObjectType()
export class User{
  @Field(()=> Int)
  @PrimaryGeneratedColumn('increment')
  id!: number;

  @Field(()=> String)
  @Column({ type: 'varchar', length: 50 })
  user_name!: string;


  @Column({ type: 'varchar', length: 10 })
  password!: string;

  @Field(()=> [Task])
  @OneToMany(()=>Task, task => task.asignedTo, { eager: true })
  tasks: Task[];

}

@InputType()
export class InputUserType {
  @Field()
  user_name!:string;

  @Field()
  password!:string;
}