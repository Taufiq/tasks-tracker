import { Args, Int, Mutation, Query, Resolver } from "@nestjs/graphql";
import { InputUserType, User } from "src/core/models/user.entity";
import { UserUseCases } from "src/core/usecases/user.usecases";

@Resolver()
export class UserResolver{

  constructor(private readonly _userUseCases: UserUseCases) {}
    
  @Query(()=>User)
  async getUser(@Args('idUser', {type:()=>Int})idUser: Number):Promise<User> {
    try {
      console.info(idUser);
      return await this._userUseCases.getUser(idUser);
    } catch (error) {
      console.log(error);
      throw error 
    }
  }

  @Mutation(()=>User)
    async createUser(@Args('user') newUser:InputUserType):Promise<User> {        
      try {
        return await this._userUseCases.createUser(newUser);
      } catch (error) {
        console.log(error);
        throw error 
      }
  }
}