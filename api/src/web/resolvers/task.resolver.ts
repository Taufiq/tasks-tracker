import { Args, Mutation, Resolver, Query, Int } from "@nestjs/graphql";
import { TaskType } from "src/core/enums/enums";
import { InputTaskType, Task, UpdateTaskType } from "src/core/models/task.entity";
import { TaskUseCases } from "src/core/usecases/task.usecases";

@Resolver(()=>Task)
export class TaskResolver{

    constructor(private readonly _taskUseCases: TaskUseCases){}

    @Query(()=>Task)
    async getTask(@Args('id',{type:()=>Int}) id: number):Promise<Task> {
      try {
        return await this._taskUseCases.getTask(id);  
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Query(()=>[Task])
    async getTaks(@Args('taskType',{nullable:true, type:()=>TaskType}) taskType:TaskType):Promise<Task[]> {
      try {
        return await this._taskUseCases.getTasks(taskType);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Mutation(()=>Task)
    async createTask(@Args('task') task:InputTaskType):Promise<Task> {        
      try {
        return await this._taskUseCases.createTask(task);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Mutation(()=>Task)
    async updateTask(@Args('task') task:UpdateTaskType):Promise<Task> {
      try {
        return await this._taskUseCases.updateTask(task);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Mutation(()=>Task)
    async changeTaskStatus(@Args('id',{type:()=>Int}) id: number):Promise<Task> {
      try {
        return await this._taskUseCases.changeTaskStatus(id);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Mutation(()=>Task)
    async asignTaskToProject(
              @Args('idTask',{type:()=>Int}) idTask : number,
              @Args('idProject', {type:()=>Int}) idProj : number) : Promise<Task> {
      try {
        return await this._taskUseCases.asignTaskToProject(idTask,idProj);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }

    @Mutation(()=>Task)
    async asignTaskToUser(
              @Args('idTask', {type:()=>Int}) idTask : number,
              @Args('idUser', {type:()=>Int} ) idUser : number) : Promise<Task> {
      try {
        return await this._taskUseCases.asignTaskToUser(idTask,idUser);
      } catch (error) {
        console.log(error);
        throw error 
      }
    }
    
}