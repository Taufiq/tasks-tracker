import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/core/models/user.entity";
import { UserUseCases } from "src/core/usecases/user.usecases";
import { UserService } from "src/data/services/user.sevice";
import { UserResolver } from "../resolvers/user.resolver";

@Module({
  imports:[TypeOrmModule.forFeature([User])],
  providers:[UserResolver,UserUseCases,UserService],
})
export class UserModule{}