import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Task } from "src/core/models/task.entity";
import { User } from "src/core/models/user.entity";
import { TaskUseCases } from "src/core/usecases/task.usecases";
import { TaskService } from "src/data/services/task.service";
import { UserService } from "src/data/services/user.sevice";
import { TaskResolver } from "src/web/resolvers/task.resolver";

@Module({
    imports:[TypeOrmModule.forFeature([Task,User])],
    providers:[TaskResolver,TaskUseCases,TaskService,UserService],
})

export class TaskModule{}