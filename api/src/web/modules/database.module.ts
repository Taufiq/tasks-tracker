import { Module } from '@nestjs/common';
import { databaseProviders } from 'src/data/services/database.service';

@Module({
  imports: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
