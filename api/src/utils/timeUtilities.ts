import { Injectable } from "@nestjs/common";

@Injectable()
export class TimeUtilities{

    /**
     * This method returns the time interval between 2 dates in a JSON format
     * { hr:0, min:0, sec:0 }
     * @param start Date 
     * @param finish Date
     * @returns  JSON
     */
    async getTimeInterval(start:Date,finish:Date):Promise<JSON> {

        let progress = { hr:0, min:0, sec:0 };
        const progressTimeInSeconds = ((finish.getTime() - start.getTime())/1000);        
        progress.hr = Math.round(progressTimeInSeconds/(3600));
        progress.min = Math.round(progressTimeInSeconds/(60) - progress.hr*60 );
        progress.sec = progressTimeInSeconds - (progress.hr*3600 + progress.min*60) ; 
        
        console.log("Progress interval from TimeUtilities :",progress);
        return JSON.parse(JSON.stringify(progress));
    }

    async addTimeTask(timeTaskToAdd: any,prevTimeTask:any):Promise<JSON>{
        if(!prevTimeTask){
            return timeTaskToAdd;
        }
        
    }
}