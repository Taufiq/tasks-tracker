import { 
    Injectable,
    NotFoundException,
    InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { plainToClass } from "class-transformer";
import { Repository } from "typeorm";
import { InputUserType, User } from "src/core/models/user.entity";
  
@Injectable()
export class UserService {
    constructor(
      @InjectRepository(User)
      private readonly _userRepository: Repository<User>,
    ) {}
  
    async get(id: Number): Promise<User> {
      let user : User | undefined
      try {
        user = await this._userRepository.findOne({
          where: { id }
        });
      } catch (error) {
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
  
      if (!user) {
        throw new NotFoundException(`User wtih id ${id} does'nt exist`);
      }
      return user;
    }
  
    async getAll():Promise<User[]>{
      try {
        return await this._userRepository.find();
      } catch (error) {
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
    }
  
    async create(user: InputUserType): Promise<User> { 
      let newUser = plainToClass(User, user)
      try {
        newUser = await this._userRepository.save(newUser)
        //const resp = plainToClass(ReadUserDto, user)
        return newUser
      }catch(error){
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
    }
  }
  