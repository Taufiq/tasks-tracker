import { 
    Injectable,
    NotFoundException,
    InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { plainToClass } from "class-transformer";
import { Repository } from "typeorm";
import {InputTaskType, Task } from "src/core/models/task.entity";
import { StatusTask, TaskType } from "src/core/enums/enums";
  
@Injectable()
export class TaskService {
    constructor(
      @InjectRepository(Task)
      private readonly _taskRepository: Repository<Task>,
    ) {}
  
    async get(taskId: number): Promise<Task> {
      let task : Task | undefined
      try {
        task = await this._taskRepository.findOne({
          where: { taskId }
        });
      } catch (error) {
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
  
      if (!task) {
        throw new NotFoundException(`Task ${taskId} doesn't exist`);
      }
      return task;
    }
  
    async getAllByType(taskType: TaskType):Promise<Task[]>{
      try {
        return await this._taskRepository.find({
            where:{type : taskType}
        });
      } catch (error) {
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
    }

    async getAll():Promise<Task[]>{
      try {
        return await this._taskRepository.find();
      } catch (error) {
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
    }
  
    async save(task: Task): Promise<Task> { 
      console.log("Task to Save before plaiToClass: ",task);
      let newTask = plainToClass(Task, task);
      console.log("task to Save: ",task);

      try {
        /*
        if(task.status===StatusTask.){
          await this._taskRepository
                .createQueryBuilder()
                .update(Task)
                .set({ status: task.status,currentTrack:'now()' })

        }*/
        newTask = await this._taskRepository.save(newTask);
        return newTask;
      }catch(error){
        const message = `we had a problem making the request to the database: ${error}`;
        throw new InternalServerErrorException({ message });
      }
    }

    
  }
  